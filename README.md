#Angular Workshop
## 1. Show the add-ons we have installed in Visual Studio Code and Atom
#### 1. Visual Studio Code
 * Angular Lenguage Service
 * TSLint
 * HTMLHint
 * Emmet
#### 2. Atom
  * atom-beautity
  * emmet
  * autocomplete
  * angular-2-typeScript
  * autocomplete-angular-material
  * linter
  * file-icons
  * color-picker
## 2. Project generation:
1. Open the terminal and move to the folder to create the application
2. Create the new application: **ng new krei-app**
3. Access the application folder: **cd krei-app**
4. Start the server: **ng serve**
5. See the result in the browser
## 3. Open the project in the editor
1. View the different items by hierarchy 
## 4. Components
1. In the terminal, open a new tab, and see that the changes are automatically reflected
2. Create a component to manage a person: **ng g component person**
3. See the new component in the IDE.
4. See that it is automatically added in **_app.module.ts_**
## 5. Embedded component
1. Inside the **_app.component.html_**, add `<app-person></app-person>` at the end
2. Show it in the browser
## 6. Component life cycle
1. When we create a component, we see that the **ngOnInit** directive is implemented 
2. Within the directive, add: `console.log(“this is ngOnInit code”)`
3. We implement the following directives:
	1. ngOnChanges
		* Within the directive we add: `console.log(“this is ngOnChanges code”)`
	2. ngAfterViewInit
		* Within the directive we add: `console.log(“this is ngAfterViewInit code”)`
	3. ngOnDestroy
		* Within the directive we add: `console.log(“this is ngOnDestroy code”)`
4. Once this is done, save and return to the browser to see the life cycle in the console.

_Note: The only one that is not seen is the one corresponding to the ngOnDestory. This is because when the component is destroyed the console is closed. This is only going to be seen after step 10 is done

## 7. Classes
1. Move in the terminal to: **cd person**
2. A class is created to represent a person: **ng g class person**
3. Go to the editor, add in **_ person.ts _** the fields first and last name as strings
4. On the **_ person.component.ts _**
5. Import the person class:
	* `import {Person} from './person';`
6. Declare the person: person: `Person = new Person ();`
## 8. One-way and two-way Data Binding
1. In the **_ app.module.ts _** we will import the **FormsModule**
	* `import {FormsModule} from '@ angular / forms';`
2. In the **_ person.component.html _**, we add labels and input to save the names and surnames, adding them as follows:
	* Name input with `[(ngModel)] =" person.name "`. Same with the surname input
3. Add the p tag to show what we are writing in the input as follows: `<p> {{person.name}} {{person.lastname}} </ p>`
4. Show the above in the browser.
5. Add a button with the event binding **(click)** calling the function **save Person**, and with the directive **[disabled]** to disable the button if you do not add a first and last name.
	* `<button (click) =" savePerson() "[disabled] ="!person.lastname || !person.name"> SAVE </ button>`
	* Create the function `savePerson()` and add `console.log (this.person)`
6. Show the above in the browser console.
7. Create a list of saved people
	* `savedPeople: Person [] = [];`
8. Edit the function `savePerson():`
	1. We add the saved person to the list of people
		* `this.savedPeople.push(this.person);`
	2. Add the following statement to empty the person variable
		* `this.person = new Person();`
9. Edit the html and add the ***ngFor** policy for saved people:
	* `* ngFor ="let person of savedPeople"`
## 9. Create Office
1. Create a component for office: **ng g component office**
2. Create a class for office: **ng g class office**
3. Add the attribute name, as string, to the office class
4. In the **_ oficina.component.ts _**
5. Import the office class:
	* `import {Office} from './office;`
6. Declare the office:
	* `office: Office = new Office();`
7. In the **_ oficina.component.html _**, create the office input, only the label and the input, with its corresponding **ngModel**.
8. Add the button with the event binding **(click)**, and with the directive **[disabled]**
	* `<button (click) =" saveOffice() "[disabled]="! office.name"> SAVE </ button>`
9. Create saved offices list
	* `savedOffices: Office [] = [];`
10. Create the function **saveOffice**
	* `this.savedOffices.push(this.office)`
	* `this.office = new Office();`
11. Edit the html and add the ***ngFor** directive for savedOffices
## 10. Routes
1. Create module with **ng g module app-routing --flat**
2. In the **_ app.module.ts _** import the newly created module **(AppRoutingModule)**
3. Edit the module **_ app.routing.ts _** adding the routes, and declaring that it is a route module
4. In the **_ app.component.html _** add below the title
	* '<Router-outlet> </ router-outlet>'
5. Links to our components
	* `<a routerLinkActive="active" [routerLink]="['/people']"> People </a>`
	* `<a routerLinkActive="active" [routerLink]="['/offices']"> Offices </a>`
## 11. Guards
1. Create class **_ app-routing-guard.ts _**
2. Edit the file of the class that implements **CanActivate**
3. Define the method:
```
	canActivate() {
		  const answer = prompt('Type the super secret code:');
		  if (answer === 'ANGULAR') {
			  return true;
		  } else {
			  console.log('Access denied, you typed: ' + answer);
			  return false;
		  }
	}
```
4. As it is an injectable, we have to add the decorator
	* `@Injectable ({providedIn: 'root'})`
5. In the **app.routing.module.ts**, add the guard `canActivate: [AppRoutingGuard]` to each of our paths
## 12. Services
1. Create service people **ng g service person**
2. Add to the service the list of people as private
3. Create setter and the getter for that list
4. Use in the component people said service:
	1. In the constructor, create a variable to invoke it
		* `constructor (private myService: PersonService)`
	2. In the **ngOnInit**, we link our list with the service
		* `this.list = this.myService.getPersons();`
	3. In the **savePerson**, we call the service to save it
		* `this.myService.savePerson(this.person);`
5. Do the same for offices
## 13. Http Queries
1. At application level, create a shared folder
2. Inside the folder, create a shared service
3. In the shared service constructor, we refer to **httpClient**
	* `constructor (private http: HttpClient) {}`
4. To do this, we will also have to import **HttpClient** at the beginning of the file
	* `import {HttpClient} from '@ angular/common/http';`
5. Also, in the **_ app.module.ts _**, we have to import the **HttpClientModule**
	* `import {HttpClientModule} from '@ angular/common/http';`
6. Generate, at the shared folder level, the country class, with the name and alpha2code attributes of type string
7. Now we are ready to make our first get. In the shared service, create a method **GetAllCountries**
	* `GetAllCountries(): Observable <Country []> {}`
8. Within the new method we add:
	* `return this.http.get <Country[]> ('https://restacountries.eu/rest/v2/all');`
9. In the person component, we initialize a list of countries, and in the **ngOnInit**, and we subscribe to the service that we just created
## 14. Npm Packages
1. In the console, execute **npm install ngx-boostrap --save**
2. The rest of the steps, we see it in the following web [npm ngx-bootstrap](https://www.npmjs.com/package/ngx-bootstrap)
3. Add **birthdate** to the class person as _date_
4. Modify to print the date of the person also in the **ngfor**
5. Import the datepicker as here [ngx-bootstrap datapicker](https://valor-software.com/ngx-bootstrap/#/datepicker)
6. Remove the class _"form-control"
## 15. Pipes
1. In the **ngfor** of person, we add the pipe date:
	* `* ngFor =" let varPersion of savedPeople | date "`
2. Create a new pipe **ng g pipe order-by** in the shared folder to generate your own pipe
3. We edit the code of that file, so it orders alphabetically (see code in the git)
4. Use it in the office component:
	1. In the decorator `@component`, after styleurls,add the pipe as provider `providers: [OrderByPipe]`
	2. Add a variable to our constructor `constructor (private officeService: OfficeService, private orderBy: OrderByPipe) {}`
	3. In the saveOffice method of the office component, edit the **saveOffice**, after saving it, we sort the list by name `this.list = this.orderBy.transform(this.list, 'name');`
## 16. Component Reutilization
1. Generate a button component in the shared folder
2. Our button will emit an event when clicked
	* `@Output() onButtonClicked: EventEmitter<any> = new EventEmitter<any>();`
3. And it will accept a Boolean to enable or disable the button
	* `@Input () disable: Boolean;`
4. For this we must import the decorators **Output** and **Input** from **@angular/core**
5. Edit the html so it looks like this
	* `<button (click) =" save () "[disabled] =" disable "> SAVE </ button>`
6. Create the method `save ()`
	* `this.onButtonClicked.emit ();`
7. We are going to use it in our component person, in the html:
	* `<app-button [disable]="! person.lastname ||! person.name" (onButtonClicked)="savePerson()"> </app-button>`
8. Do the same with office.
## 17. Two TS using the same HTML
1. In the folder of the office component, create a file called **_office-admin.component.ts_**
2. Copy the code from **_oficina.component.ts**, modifying the following:
	* name of the class: **OfficeAdminComponent**
	* name of the selector (in the decorator): **app-oficina-admin**
3. Remove from the office component the save method
4. Add to both components the variable **isAdmin**, being true in one and false in the other
5. In the template, in the input and the save button, we add an `*ngIf="isAdmin"` to be shown only in the corresponding component
6. In the **_app.component.html_** add a new link
	* `<a routerLinkActive="active" [routerLink]="['/officeadmin']"> ADMIN Offices </a>`
7. In our route module, we declare a new
	* `{path: 'officeadmin', component: OfficeAdminComponent}`
## 18. Add a jQuery
1. In console run the following command **npm install jquery --save**
2. In the file **_angular.json_**, in the scripts node, reference the newly installed javascript to be able to use it
	* `" scripts": [" node_modules / jquery / dist / jquery.min.js "]`
3. In the **_app.component.ts_**, declare the variable `jQuery: declare var jQuery: any;`
4. In the main http, add the button
	* `<span (click)="goUp()"class="go-up icon-arrow-up2 "title=" Go up "> <img class =" img-arrow " src ="assets/img/scroll.png"> </ span>`
5. Find an image that represents the button and paste it into **_assets/img/scroll.png_**
6. Finally, in the app component, create the method **goUp**:


```
	 goUp(){
	 jQuery('body, html').animate({
		  scrollTop: '0px'
		}, 300);
	 }
```
* _add a little css…_
```
	 .icon-arrow-up2 {
		display: block;
		padding: 20px;
		background: transparent;
		font-size: 20px;
		color: #fff;
		cursor: pointer;
		position: fixed;
		bottom: 20px;
		right: 20px;
	 }
	 .img-arrow {
		width: 30px;
		height: 30px;
	 }
```
## 19. Compile
1. In the console, excecute ng build --prod
