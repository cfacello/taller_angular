import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.css']
})
export class ButtonComponent implements OnInit {

  @Output() buttonClicked: EventEmitter<String> = new EventEmitter<String>();
  @Input() disable: Boolean;

  constructor() { }

  ngOnInit() {
  }

  guardar() {
    this.buttonClicked.emit('Foo');
  }
}
