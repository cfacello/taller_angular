import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PersonasComponent } from './personas/personas.component';
import { OficinaComponent } from './oficina/oficina.component';
import { AppRoutingGuard } from './app-routing-guard';
import { OficinaAdminComponent } from './oficina/oficina-admin.component';

const routes: Routes = [
  { path: 'personas', component: PersonasComponent },
  { path: 'oficinas', component: OficinaComponent },
  { path: 'oficinasadmin', component: OficinaAdminComponent, canActivate: [AppRoutingGuard] }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
