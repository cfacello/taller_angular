import { Component, OnInit } from '@angular/core';
import { Oficina } from './oficina';
import { OficinasService } from './oficinas.service';

@Component({
  selector: 'app-oficina',
  templateUrl: './oficina.component.html',
  styleUrls: ['./oficina.component.css']
})
export class OficinaComponent implements OnInit {

  oficina: Oficina = new Oficina();
  listaOficinas: Oficina[] = [];
  isAdmin = false;


  constructor(private oficinaService: OficinasService) { }

  ngOnInit() {
    this.listaOficinas = this.oficinaService.getOficinas();
  }

}
