import { Injectable } from '@angular/core';
import { Oficina } from './oficina';

@Injectable({
  providedIn: 'root'
})
export class OficinasService {

  private listaOficinas: Oficina[] = [];

  constructor() { }

  guardarOficina(oficina: Oficina) {
    this.listaOficinas.push(oficina);
  }

  getOficinas(): Oficina[] {
    return this.listaOficinas;
  }
}
