import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

import { AppComponent } from './app.component';
import { PersonasComponent } from './personas/personas.component';
import { OficinaComponent } from './oficina/oficina.component';
import { ButtonComponent } from './shared/button/button.component';
import { OficinaAdminComponent } from './oficina/oficina-admin.component';


@NgModule({
  declarations: [
    AppComponent,
    PersonasComponent,
    OficinaComponent,
    ButtonComponent,
    OficinaAdminComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    BsDatepickerModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
